;;;

(in-package "SALZA2")

(define-compiler-macro match-length (p1 p2 input end)
  `(let ((p1 ,p1)
         (p2 ,p2)
         (input ,input)
         (end ,end))
     (declare (type input-index p1 p2 end)
              (type input-buffer input)
              (optimize speed))
     (let ((length 0))
       (loop
         (when (or (/= (aref input p1) (aref input p2))
                   (= length +maximum-match-length+)
                   (= p1 end))
           (return length))
         (setf p1 (logand (1+ p1) #xFFFF)
               p2 (logand (1+ p2) #xFFFF)
               length (logand #xFFF (the fixnum (1+ length))))))))

(define-compiler-macro longest-match (p1 input chains end max-tests)
  `(let ((p1 ,p1)
         (input ,input)
         (chains ,chains)
         (end ,end)
         (max-tests ,max-tests))
     (declare (type input-index p1 end)
              (type input-buffer input)
              (type chains-buffer chains)
              (type (integer 0 32) max-tests)
              (optimize speed))
     (let ((match-length 0)
           (p2 (aref chains p1))
           (test-count 0)
           (distance 0))
       (declare (type (integer 0 258) match-length)
                (type (integer 0 32) test-count))
       (loop
         (when (or (= match-length +maximum-match-length+)
                   (= test-count max-tests)
                   (= p2 p1)
                   (= p2 (aref chains p2)))
           (return (values match-length distance)))
         (let ((step (logand (- p1 p2) #xFFFF)))
           (when (< +maximum-match-distance+ step)
             (return (values match-length distance)))
           (let ((possible-length (match-length p1 p2 input end)))
             (when (and (< 2 possible-length)
                        (< match-length possible-length))
               (setf distance step
                     match-length possible-length))
             (setf p2 (aref chains p2)))
           (incf test-count))))))

(defun compress (input chains start end
                 literal-fun length-fun distance-fun)
  (declare (type input-buffer input)
           (type chains-buffer chains)
           (type input-index start end)
           (type function literal-fun length-fun distance-fun)
           (optimize speed)
           (:explain :boxing :inlining))

  (let ((p start))
    (loop
     (when (= p end)
       (return))
     (multiple-value-bind (length distance)
         (longest-match p input chains end 4)
       (declare (type (integer 0 258) length)
                (type (integer 0 32768) distance))
       (cond ((zerop length)
              (funcall literal-fun (aref input p))
              (setf p (logand (the fixnum (+ p 1)) #xFFFF)))
             (t
              (funcall length-fun length)
              (funcall distance-fun distance)
              (setf p (logand (the fixnum (+ p length)) #xFFFF))))))))

(defun merge-bits (code size buffer bits callback)
  (declare (type (unsigned-byte 32) code)
           (type (integer 0 32) size)
           (type bitstream-buffer-bit-count bits)
           (type bitstream-buffer buffer)
           (type function callback)
           (optimize speed)
           (:explain :boxing :inlining))
  ;; BITS represents how many bits have been added to BUFFER so far,
  ;; so the FLOOR of it by 8 will give both the buffer byte index and
  ;; the bit index within that byte to where new bits should be
  ;; merged
  (let ((buffer-index (ash bits -3))
        (bit (logand #b111 bits)))
    ;; The first byte to which new bits are merged might have some
    ;; bits in it already, so pull it out for merging back in the
    ;; loop. This only has to be done for the first byte, since
    ;; subsequent bytes in the buffer will consist solely of bits from
    ;; CODE.
    ;;
    ;; The check (PLUSP BIT) is done to make sure that no garbage bits
    ;; from a previous write are re-used; if (PLUSP BIT) is zero, all
    ;; bits in the first output byte come from CODE.
    (let ((merge-byte (if (plusp bit) (aref buffer buffer-index) 0))
          (end #.+bitstream-buffer-size+)
          (result (+ bits size)))
      ;; (ceiling (+ bit size) 8) is the total number of bytes touched
      ;; in the buffer
      (flet ((ceiling (number divisor)
               (loop for i from 0
                   while (> number 0) do
                     (decf number divisor)
                   finally (return i))))
        (dotimes (i (the fixnum (ceiling (+ bit size) 8)))
          (let ((shift (+ bit (* i -8)))
                (j (+ buffer-index i)))
            (declare (type fixnum shift i j))
            ;; Buffer filled up in the middle of CODE
            (when (= j end)
              (funcall callback buffer j))
            ;; Merge part of CODE into the buffer
            (setf (aref buffer (logand (the fixnum #.+bitstream-buffer-mask+) j))
              (logior (logand #xFF (the fixnum (ash code (the (integer -63 63) shift)))) merge-byte))
            (setf merge-byte 0))))
      ;; Writing is done, and the buffer is full, so call the callback
      (when (= result #.+bitstream-buffer-bits+)
        (funcall callback buffer #.+bitstream-buffer-size+))
      ;; Return only the low bits of the sum
      (logand (the fixnum #.+bitstream-buffer-bitmask+) result))))

(define-compiler-macro mod8191 (z)
  `(let ((z ,z))
     (declare (type (integer 0 3057705) z))
     (let ((zz (+ (ash z -13) (logand #x1FFF z))))
       (if (< zz #x1FFF)
           zz
         (- zz #x1FFF)))))

(defun update-chains (input hashes chains start count)
  (declare (type input-buffer input)
           (type hashes-buffer hashes)
           (type chains-buffer chains)
           (type input-index start)
           (type (integer 0 32768) count)
           (optimize speed)
           (:explain :boxing :inlining))
  (when (< count 3)
    (return-from update-chains))
  (let* ((hash (hash-value input start))
         (p0 start)
         (p1 (logand (the fixnum (+ start 2)) #xFFFF)))
    (declare (type (integer 0 3057705) hash))
    (loop
      (let ((hash-index (mod8191 hash)))
        ;; Stuff the old hash index into chains at p0
        (setf (aref chains p0) (aref hashes hash-index))
        ;; Stuff p0 into the hashes
        (setf (aref hashes hash-index) p0)
        ;; Tentatively advance; if we hit the end, don't do the rest of
        ;; the hash update
        (setf p1 (logand (the fixnum (1+ p1)) #xFFFF))
        (decf count)
        (when (= count 2)
          (return))
        ;; We're not at the end, so lop off the high, shift left, and
        ;; add the low to form a new hash value
        (setf hash (- hash (* (aref input p0) 11881)))
        (setf hash (* hash 109))
        (setf p0 (logand (the fixnum (1+ p0)) #xFFFF))
        (setf hash (+ hash (aref input p1)))))))

;;;
